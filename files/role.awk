BEGIN {
  FS="[. ]+"
  role = ""
  printnext = "false"
}
/1.3.6.1.4.1.34380.1.1.13:/ {
  printnext = "true"
  next
}
{
  if (printnext == "true") {
    print $2
    exit
  }
}