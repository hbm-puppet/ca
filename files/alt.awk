BEGIN {
  RS=",";
  count=2;
  print "subjectAltName = @alt_names";
  print "[alt_names]";
  print "DNS.1=" fqdn;
}
{
  print "DNS." count "=" $1;
  count++;
}